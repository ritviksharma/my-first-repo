

#lang racket
(require rackunit)
(require racket/list)
;;;Purpose(ring-area) - ring-area takes two arguments (outer-radius and inner-radius) and returns the area of the ring.
;;;Signature: Number? Number? -> Number?
;;;Examples : (1).ring-area(2 1)=>9.42
;;;           (2).ring-area(3 2)=>15.70
;;;Tests: (check-=(ring-area 2 1) 9.42 0.1)
;;;       (check-=(ring-area 3 2) 15.70 0.1)
;;; Definition
(define ring-area
  ( lambda
       (outer-radius inner-radius)        (* 3.14 (- (* outer-radius outer-radius) (* inner-radius inner-radius))))) (provide ring-area)




;;;Purpose(list-with-length-at-least-two?) -returns true if the list has at least two elements, false otherwise.
;;;Signature: List? -> True or False
;;;Examples : (1).( list-with-length-at-least-two '(a b))=>#t
;;;           (2).( list-with-length-at-least-two '(a))=>#f
;;;Definition
(define length   ;;;Function to calculate length of the list
  (lambda
      (lst)       ( if(null? lst) 0
                      ( + 1 (length(rest lst))))))
(provide length)
(define (list-with-length-at-least-two lst) ;;;Driver function to test if length has more than 1 elements
  ( if( > (length lst) 1)
      #t
      #f))
(provide list-with-length-at-least-two)



;;;Purpose(remove-second)- returns a list without its second element
;;;Signature: List?->List? ;;;Examples: (1).(remove-second '(1 2 3))=>'(1 3)
;;;          (2).(remove-second '(1 2 3 4))=>'(1 3 4)
;;;Definition
(define remove-second
  (lambda
      (ls)     (cons (first ls) (rest (rest ls)))))
(provide remove-second)




;;;Purpose(list-length)- Returns the length of the list
;;;Signature: List?-Number?
;;;Examples: (1).(list-length '(1 2 3))=>3
;;;          (2).(list-length '(1 2 3 4))=>4
;;;Definition
(define list-length
  ( lambda
       (lst)        ( if(null? lst) 0
                        (+ 1 list-length(rest lst)))))
(provide list-length)





;;;Purpose(gcd) - Returns the greatest common divisor of two numbers
;;;Signature: Number? Number?-> Number?
;;;Examples: (1).(gcd 1 2)=>1
;;;          (2).(gcd 2 3)=>1
;;;Defintion
(define
  (gcd a b)     ( cond [(= b 0) a]
                       [else (gcd b (modulo a b))]))
(provide gcd)

